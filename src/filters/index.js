import currencyFilter from './currency.filter'

export default {
  install: function (Vue) {
    Vue.filter('currency', currencyFilter)
  }
}
