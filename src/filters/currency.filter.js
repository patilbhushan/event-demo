export default function currencyFilter (value, symbol = 'RM') {
    return `${symbol}${' '}${value}`
  }
